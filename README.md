# Programmers test

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Programmers test's is a CRUD;

# New Features!

  - Search users
  - Add users
  - Edit users
  - Delete users

### Tech

Technologies used

* [Angular 7]
* [Swagger]
* [C#].
* [WebApi 2]
* [Sass]
* [SqlServer]
* [Angular cli]
* [FLuentValidation]
* [AutoMapper]
* [Dapper]
* [Dapper extensions]
* [Unit of work]
* [Simple injector]

### Installation

Programmers test requires [Node.js](https://nodejs.org/) v10+, angular cli, IIS and sqlserver to run.

Install the dependencies and devDependencies and start the server.

```sh
npm install npm@latest -g
npm install -g angular-cli
npm install
```

#### Building for source front end
```sh
ng s --o
```

#### Building for source back end

```sh
Compile in visual studio 2017
```

Thank you!!!