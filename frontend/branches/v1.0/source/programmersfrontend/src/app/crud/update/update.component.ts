import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { IUser } from '../../model/user.interface';
import { FormControl, Validators } from '@angular/forms';
import { IValidationResult } from '../../model/validation.result.interface';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  data: IUser = {} as IUser;

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  constructor(
    private dataService: DataService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.dataService.userEdit.subscribe(iUser => {
      this.data = iUser;
    });
  }

  ngOnInit() {}

  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Campo obrigatório'
      : this.formControl.hasError('email')
      ? 'E-mail inválido'
      : '';
  }

  save() {
    this.dataService.updateIUser(this.data).subscribe((response: IValidationResult) => {
      if (response.Errors !== null && response.Errors !== undefined && response.Errors.length > 0) {
        response.Errors.map(error => {
          this.toastr.error('Error', error.Message);
        });
      } else {
        this.toastr.success('Success', 'User save with success!');

        this.router.navigate(['/home']);
      }
    });
  }
}
