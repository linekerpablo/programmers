import { Component, OnInit } from '@angular/core';
import { IUser } from '../../model/user.interface';
import { FormControl, Validators } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IValidationResult } from '../../model/validation.result.interface';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  data: IUser = {} as IUser;

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  constructor(private dataService: DataService, private router: Router, private toastr: ToastrService) {}

  ngOnInit() {}

  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Field required'
      : this.formControl.hasError('email')
      ? 'E-mail invalid'
      : '';
  }

  save() {
    this.dataService.addIUser(this.data).subscribe((response: IValidationResult) => {
      if (response.Errors !== null && response.Errors !== undefined && response.Errors.length > 0) {
        response.Errors.map(error => {
          this.toastr.error('Error', error.Message);
        });
      } else {
        this.toastr.success('Success', 'User save with success!');

        this.router.navigate(['/home']);
      }
    });
  }
}
