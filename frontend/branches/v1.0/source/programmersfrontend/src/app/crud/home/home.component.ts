import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { IUser } from './../../model/user.interface';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { MatSort, MatDialog, MatPaginator } from '@angular/material';
import { map } from 'rxjs/operators';
import { DataService } from './../../services/data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DeleteComponent } from './../delete/delete.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displayedColumns = ['Id', 'Name', 'Email', 'Telephone', 'actions'];
  dataBase: DataService | null;
  dataSource: UserDataSource | null;
  index: number;
  id: number;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: DataService,
    private router: Router
  ) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  loadData() {
    this.dataBase = new DataService(this.httpClient);
    this.dataSource = new UserDataSource(this.dataBase, this.paginator, this.sort);
    console.log(this.dataSource);
    fromEvent(this.filter.nativeElement, 'keyup').subscribe(() => {
      if (!this.dataSource) {
        return;
      }
      this.dataSource.filter = this.filter.nativeElement.value;
    });
  }

  update(iUser: IUser) {
    this.dataService.userEdit.next(iUser);

    this.router.navigate(['/update']);
  }

  deleteItem(iUser: IUser) {
    this.dataService.userEdit.next(iUser);

    this.router.navigate(['/delete']);
  }
}

export class UserDataSource extends DataSource<IUser> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: IUser[] = [];
  renderedData: IUser[] = [];

  constructor(
    public _database: DataService,
    public _paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();

    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<IUser[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._database.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._database.getAllIUsers();

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this._database.data.slice().filter((iUser: IUser) => {
          const searchStr = (iUser.Id + iUser.Name + iUser.Email + iUser.Telephone).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      })
    );
  }

  disconnect() {}

  /** Returns a sorted copy of the database data. */
  sortData(data: IUser[]): IUser[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.Id, b.Id];
          break;
        case 'name':
          [propertyA, propertyB] = [a.Name, b.Name];
          break;
        case 'email':
          [propertyA, propertyB] = [a.Email, b.Email];
          break;
        case 'Telephone':
          [propertyA, propertyB] = [a.Telephone, b.Telephone];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
