import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DataService } from '../../services/data.service';
import { IValidationResult } from '../../model/validation.result.interface';
import { Router } from '@angular/router';
import { IUser } from '../../model/user.interface';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  data: IUser = {} as IUser;

  constructor(
    private dataService: DataService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.dataService.userEdit.subscribe(iUser => {
      this.data = iUser;
    });
  }

  ngOnInit() {}

  confirmDelete() {
    this.dataService.deleteIUser(this.data.Id).subscribe((response: IValidationResult) => {
      this.toastr.success('Success', 'User deleted with success!');

      this.router.navigate(['/home']);
    });
  }
}
