export interface IUser {
  Id: number;
  Name: string;
  Email: string;
  Telephone: string;
}
