export interface Error {
  Message: string;
}

export interface IValidationResult {
  IsValid: boolean;
  Errors: Error[];
}
