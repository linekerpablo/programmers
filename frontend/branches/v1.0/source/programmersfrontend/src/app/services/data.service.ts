import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { IUser } from '../model/user.interface';
import { map, tap, catchError } from 'rxjs/operators';
import { IValidationResult } from '../model/validation.result.interface';

@Injectable()
export class DataService {
  private readonly API_URL = 'http://localhost:51308/api/User/';
  private readonly SAVE = 'Save/';
  private readonly UPDATE = 'Update/';
  private readonly DELETE = 'Delete/';
  private readonly GETALL = 'GetAll/';

  dataChange: BehaviorSubject<IUser[]> = new BehaviorSubject<IUser[]>([]);
  userEdit: BehaviorSubject<IUser> = new BehaviorSubject<IUser>({} as IUser);

  constructor(private httpClient: HttpClient) {}

  get data(): IUser[] {
    return this.dataChange.value;
  }

  getAllIUsers(): void {
    this.httpClient.get<IUser[]>(this.API_URL + this.GETALL).subscribe(
      data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }

  addIUser(iUser: IUser): Observable<IValidationResult> {
    return this.httpClient.post<IValidationResult>(this.API_URL + this.SAVE, iUser).pipe(
      catchError(err => {
        return throwError(err);
      }),
      map((data: IValidationResult) => {
        return data;
      })
    );
  }

  updateIUser(iUser: IUser): Observable<IValidationResult> {
    return this.httpClient.put<IValidationResult>(this.API_URL + this.UPDATE, iUser).pipe(
      catchError(err => {
        return throwError(err);
      }),
      map((data: IValidationResult) => {
        return data;
      })
    );
  }

  deleteIUser(id: number): Observable<IValidationResult> {
    return this.httpClient.delete<IValidationResult>(this.API_URL + this.DELETE + `${id}`).pipe(
      catchError(err => {
        return throwError(err);
      }),
      map((data: IValidationResult) => {
        return data;
      })
    );
  }
}
