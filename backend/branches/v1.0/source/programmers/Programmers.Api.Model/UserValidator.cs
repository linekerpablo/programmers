﻿using FluentValidation;
using Programmers.Api.Model.Request;

namespace Programmers.Api.Model
{
    public class UserValidator : AbstractValidator<UserModelRequest>
    {
        public UserValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name must be informed.");
            RuleFor(x => x.Email).EmailAddress().WithMessage("E-mail invalid.").NotEmpty().WithMessage("Telephone must be informed.");
            RuleFor(x => x.Telephone).MinimumLength(11).WithMessage("Telephone invalid.").NotEmpty().WithMessage("Telephone must be informed.");
        }
    }
}
