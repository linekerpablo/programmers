﻿using FluentValidation.Attributes;

namespace Programmers.Api.Model.Request
{
    [Validator(typeof(UserValidator))]
    public class UserModelRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
    }
}
