﻿namespace Programmers.Api.Model.Response
{
    public class UserModelResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
    }
}
