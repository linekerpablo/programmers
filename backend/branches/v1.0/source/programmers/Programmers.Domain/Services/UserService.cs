﻿using Programmers.Domain.Entities;
using Programmers.Domain.Interfaces.Repository;
using Programmers.Domain.Interfaces.Services;
using Programmers.Domain.Services.Common;
using Programmers.Domain.Validations;
using System.Data;
using System.Linq;

namespace Programmers.Domain.Services
{
    public class UserService : Service<User>, IUserService
    {
        IUserRepository _userRepository;
        private readonly ValidationResult _validationResult = new ValidationResult();

        public UserService(IUserRepository userRepository) : base(userRepository)
        {
            _userRepository = userRepository;
        }

        protected ValidationResult ValidationResult
        {
            get { return _validationResult; }
        }

        public ValidationResult Add(User entity, IDbTransaction transaction = null, int? commandTimeout = default(int?))
        {
            User user = _userRepository.GetByObject(new { Email = entity.Email }, transaction: transaction).FirstOrDefault();

            if (user != null)
               return _validationResult.Add("E-mail already registered.");

            var add = _userRepository.Add<User>(entity, transaction);

            if (add == null)
                _validationResult.Add("Entity is null, try again!" + entity + " Save");

            return _validationResult;
        }
    }
}
