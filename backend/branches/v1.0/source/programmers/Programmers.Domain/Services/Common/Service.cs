﻿using System.Collections.Generic;
using System.Data;
using Programmers.Domain.Interfaces.Services.Common;
using Programmers.Domain.Interfaces.Repository.Common;
using Programmers.Domain.Validations;
using Programmers.Domain.Interfaces.Validations;

namespace Programmers.Domain.Services.Common
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {
        private readonly IRepository<TEntity> _Repository;
        private readonly ValidationResult _validationResult;

        public Service(IRepository<TEntity> Repository)
        {
            _Repository = Repository;
        }

        public ValidationResult Add<T>(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = default(int?))
        {
            var selfValidationEntity = entity as ISelfValidation;

            if (selfValidationEntity != null && !selfValidationEntity.IsValid)
                return selfValidationEntity.ValidationResult;

            var add = _Repository.Add<T>(entity, transaction);

            if (add == null)
                _validationResult.Add("Entity is null, try again!" + entity + " Save");

            return _validationResult;
        }

        public ValidationResult Update(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = default(int?))
        {
            var selfValidationEntity = entity as ISelfValidation;

            if (selfValidationEntity != null && !selfValidationEntity.IsValid)
                return selfValidationEntity.ValidationResult;

            var update = _Repository.Update(entity, transaction);

            if (!update)
                _validationResult.Add("Entity is null, try again!" + entity + " Update");

            return _validationResult;
        }

        public ValidationResult Delete(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = default(int?))
        {
            var delete = _Repository.Delete(entity, transaction);

            if (!delete)
                _validationResult.Add("Entity is null, try again!" + entity + " Delete");

            return _validationResult;
        }

        public TEntity GetById<T>(int id, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return _Repository.GetById<T>(id);
        }

        public IEnumerable<TEntity> GetAll(IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return _Repository.GetAll();
        }

        public IEnumerable<TEntity> GetByObject(object @where = null, object order = null, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return _Repository.GetByObject(@where);
        }
    }
}
