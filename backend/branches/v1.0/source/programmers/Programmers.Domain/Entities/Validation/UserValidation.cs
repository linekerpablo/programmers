﻿using Programmers.Domain.Entities;
using Programmers.Domain.Validations;
using Programmers.Dominio.Entities.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programmers.Dominio.Entities.Validation
{
    public class UserValidation : Validation<User>
    {
        public UserValidation()
        {
            AddRule(new ValidationRule<User>(new EmailCheckSpecification(), "E-mail digitado já existe."));
        }
    }
}
