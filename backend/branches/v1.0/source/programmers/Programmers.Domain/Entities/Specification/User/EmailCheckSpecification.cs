﻿using Programmers.Domain.Interfaces.Specification;
using Programmers.Domain.Entities;

namespace Programmers.Dominio.Entities.Specification
{
    public class EmailCheckSpecification : ISpecification<User>
    {
        public bool IsSatisfiedBy(User entity)
        {
            return false;
        }
    }
}
