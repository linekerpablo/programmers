﻿using Programmers.Domain.Validations;

namespace Programmers.Domain.Interfaces.Validations
{
    public interface ISelfValidation
    {
        ValidationResult ValidationResult { get; }
        bool IsValid { get; }
    }
}
