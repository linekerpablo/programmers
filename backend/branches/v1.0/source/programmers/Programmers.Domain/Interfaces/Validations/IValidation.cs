﻿using Programmers.Domain.Validations;

namespace Programmers.Domain.Interfaces.Validations
{
    public interface IValidation<in TEntity>
    {
        ValidationResult Valid(TEntity entity);
    }
}
