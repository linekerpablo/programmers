﻿using Programmers.Domain.Validations;
using System.Collections.Generic;
using System.Data;

namespace Programmers.Domain.Interfaces.Repository.Common
{
    public interface IRepository<TEntity> where TEntity : class
    {
        dynamic Add<T>(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null);
        bool Update(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null);
        bool Delete(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null);
        TEntity GetById<T>(int id, IDbTransaction transaction = null, int? commandTimeout = null);
        IEnumerable<TEntity> GetAll(IDbTransaction transaction = null, int? commandTimeout = null);
        IEnumerable<TEntity> GetByObject(object where = null, object order = null, IDbTransaction transaction = null, int? commandTimeout = null);
    }
}
