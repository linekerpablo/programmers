﻿using Programmers.Domain.Entities;
using Programmers.Domain.Interfaces.Repository.Common;

namespace Programmers.Domain.Interfaces.Repository
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
