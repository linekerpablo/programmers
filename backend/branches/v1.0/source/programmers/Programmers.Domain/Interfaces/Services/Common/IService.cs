﻿using Programmers.Domain.Validations;
using System.Collections.Generic;
using System.Data;

namespace Programmers.Domain.Interfaces.Services.Common
{
    public interface IService<TEntity> where TEntity : class
    {
        ValidationResult Add<T>(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null);
        ValidationResult Update(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null);
        ValidationResult Delete(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null);
        TEntity GetById<T>(int id, IDbTransaction transaction = null, int? commandTimeout = null);
        IEnumerable<TEntity> GetAll(IDbTransaction transaction = null, int? commandTimeout = null);
        IEnumerable<TEntity> GetByObject(object where = null, object order = null, IDbTransaction transaction = null, int? commandTimeout = null);
    }
}
