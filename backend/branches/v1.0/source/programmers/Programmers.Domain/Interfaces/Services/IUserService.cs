﻿using Programmers.Domain.Entities;
using Programmers.Domain.Interfaces.Services.Common;
using Programmers.Domain.Validations;
using System.Data;

namespace Programmers.Domain.Interfaces.Services
{
    public interface IUserService : IService<User>
    {
        ValidationResult Add(User entity, IDbTransaction transaction = null, int? commandTimeout = null);
    }
}
