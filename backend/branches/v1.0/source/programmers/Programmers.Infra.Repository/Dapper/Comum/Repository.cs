﻿using Programmers.Domain.Interfaces.Repository.Common;
using Programmers.Infra.Context.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using DapperExtensions;
using DapperExtensions.Sql;

namespace Programmers.Infra.Repository.Dapper.Common
{
    public class Repository<TEntity> : IRepository<TEntity>, IDisposable where TEntity : class
    {
        public IDbConnection Conn { get; set; }
        
        public Repository(IDapperContext context)
        {
            Conn = context.Connection;
            InicializaMapperDapper();
        }

        public static void InicializaMapperDapper()
        {
        }

        public dynamic Add<T>(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return entity == null ? null : Conn.Insert(entity, transaction, commandTimeout);
        }

        public bool Update(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return entity != null && Conn.Update(entity, transaction, commandTimeout);
        }

        public bool Delete(TEntity entity, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return entity != null && Conn.Delete(entity, transaction, commandTimeout);
        }

        public TEntity GetById<T>(int id, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return Conn.Get<TEntity>(id, transaction, commandTimeout);
        }

        public IEnumerable<TEntity> GetAll(IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return Conn.GetList<TEntity>(null, null, transaction, commandTimeout);
        }

        public IEnumerable<TEntity> GetByObject(object @where = null, object order = null, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return Conn.GetList<TEntity>(@where, null, transaction, commandTimeout);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
