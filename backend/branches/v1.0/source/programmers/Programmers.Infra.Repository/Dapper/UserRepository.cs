﻿using Programmers.Domain.Entities;
using Programmers.Domain.Interfaces.Repository;
using Programmers.Infra.Context.Interfaces;
using Programmers.Infra.Repository.Dapper.Common;

namespace Programmers.Infra.Repository.Dapper
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IDapperContext context) : base(context)
        {
        }
    }
}
