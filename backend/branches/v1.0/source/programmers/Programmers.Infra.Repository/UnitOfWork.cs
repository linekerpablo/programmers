﻿using CloudEmiNfeSat_Backend.Dominio.Interfaces.Repositorio;
using CloudEmiNfeSat_Backend.Dominio.Interfaces.Servicos;
using CloudEmiNfeSat_Backend.Infra.Repositorio.Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace CloudEmiNfeSat_Backend.Infra.Repositorio
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;

        public IDbTransaction _transaction { get; private set; }
        private IDbConnection _connection;

        private ICfopRepositorio _cfopRepositorio;
        public ICfopRepositorio CfopRepositorio
        {
            get => _cfopRepositorio ?? (_cfopRepositorio = new CfopRepositorio(_transaction));
            set => _cfopRepositorio = value;
        }

        public UnitOfWork(string connectionString)
        {
            _connection = new MySqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public static IUnitOfWork Create(string connectionString)
        {
            return new UnitOfWork(connectionString);
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
            }
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }
    }
}
