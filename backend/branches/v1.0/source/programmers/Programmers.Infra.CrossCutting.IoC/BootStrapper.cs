using Programmers.Aplication;
using Programmers.Aplication.Interfaces;
using Programmers.Domain.Interfaces.Services;
using Programmers.Domain.Interfaces.Repository;
using Programmers.Domain.Services;
using Programmers.Infra.Context;
using Programmers.Infra.Context.Interfaces;
using Programmers.Infra.Repository.Dapper;
using SimpleInjector;
using Programmers.Api.Model;

namespace Programmers.Infra.CrossCutting.IoC
{
    public static class BootStrapper
    {
        public static void Register(Container container)
        {
            container.Register<IUserAppService, UserAppService>(Lifestyle.Scoped);
            container.Register<IUserService, UserService>(Lifestyle.Scoped);
            container.Register<IUserRepository, UserRepository>(Lifestyle.Scoped);

            container.Register<IDapperContext, DapperContext>(Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
        }
    }
}

