﻿using Programmers.Domain.Validations;

namespace Programmers.Aplication
{
    public class AppService
    {
        public AppService()
        {
            ValidationResult = new ValidationResult();
        }
        protected ValidationResult ValidationResult { get; private set; }
    }
}
