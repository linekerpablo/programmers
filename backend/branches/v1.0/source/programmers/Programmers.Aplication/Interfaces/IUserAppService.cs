﻿using Programmers.Aplication.Interfaces.Common;
using Programmers.Domain.Entities;
using Programmers.Domain.Validations;

namespace Programmers.Aplication.Interfaces
{
    public interface IUserAppService : IAppService<User>
    {
        ValidationResult Add(User user);
    }
}
