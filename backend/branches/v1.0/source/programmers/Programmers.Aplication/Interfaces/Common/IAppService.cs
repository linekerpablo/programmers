﻿using Programmers.Domain.Validations;
using System;
using System.Collections.Generic;

namespace Programmers.Aplication.Interfaces.Common
{
    public interface IAppService<TEntity> : IDisposable where TEntity : class
    {
        TEntity GetById<T>(int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetByObject(object @where = null, object order = null);
        ValidationResult Add<T>(TEntity entity);
        ValidationResult Update(TEntity entity);
        ValidationResult Delete(TEntity entity);
    }
}
