﻿using System;
using System.Collections.Generic;
using Programmers.Aplication;
using Programmers.Aplication.Interfaces;
using Programmers.Domain.Entities;
using Programmers.Domain.Interfaces.Services;
using Programmers.Domain.Validations;
using Programmers.Infra.Context.Interfaces;

namespace Programmers.Aplication
{
    public class UserAppService : AppService, IUserAppService
    {
        private readonly IUserService _userService;
        private readonly IUnitOfWork _uow;

        public UserAppService(IUserService userService, IUnitOfWork uow)
        {
            _userService = userService;
            _uow = uow;
        }

        public ValidationResult Add(User entity)
        {
            ValidationResult.Add(_userService.Add(entity, _uow.BeginTransaction()));

            if (ValidationResult.IsValid)
                _uow.Commit();

            return ValidationResult;
        }

        public ValidationResult Update(User entity)
        {
            ValidationResult.Add(_userService.Update(entity, _uow.BeginTransaction()));

            if (ValidationResult.IsValid)
                _uow.Commit();

            return ValidationResult;
        }

        public ValidationResult Delete(User entity)
        {
            ValidationResult.Add(_userService.Delete(entity, _uow.BeginTransaction()));

            if (ValidationResult.IsValid)
                _uow.Commit();

            return ValidationResult;
        }

        public IEnumerable<User> GetAll()
        {
            return _userService.GetAll();
        }

        public User GetById<T>(int id)
        {
            return _userService.GetById<T>(id);
        }

        public IEnumerable<User> GetByObject(object where = null, object order = null)
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public ValidationResult Add<T>(User entity)
        {
            throw new NotImplementedException();
        }
    }
}
