﻿using System;
using System.Data;

namespace Programmers.Infra.Context.Interfaces
{
    public interface IDapperContext : IDisposable
    {
        IDbConnection Connection { get; }
    }
}
