﻿using System;
using System.Data;

namespace Programmers.Infra.Context.Interfaces
{
    public interface IUnitOfWork 
    {
        IDapperContext Context { get; }
        IDbTransaction Transaction { get; }
        IDbTransaction BeginTransaction();
        void Commit();
        void Rollback();
    }
}
