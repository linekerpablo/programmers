﻿using Programmers.Infra.Context.Interfaces;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace Programmers.Infra.Context
{
    public class DapperContext : IDapperContext
    {
        private readonly string _connectionString;
        private IDbConnection _connection;
        
        public DapperContext()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
        }

        public IDbConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new SqlConnection(_connectionString);
                }

                if (_connection.State != ConnectionState.Open)
                {
                    _connection.Open();
                }

                return _connection;
            }
        }

        public void Dispose()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
                _connection.Close();
        }
    }
}
