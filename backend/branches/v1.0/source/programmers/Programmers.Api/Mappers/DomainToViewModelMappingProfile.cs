using AutoMapper;
using Programmers.Api.Model.Request;
using Programmers.Domain.Entities;

namespace Programmers.Api.Mappers 
{ 
    public class DomainToViewModelMappingProfile : Profile
     { 
          public DomainToViewModelMappingProfile() 
          {
            CreateMap<User, UserModelRequest>();
        } 
     } 
} 

