﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.Owin;
//using Microsoft.Owin.Security.Cookies;
//using Microsoft.Owin.Security.Google;
//using Microsoft.Owin.Security.OAuth;
//using Owin;
//using JsonWebToken.Providers;
//using JsonWebToken.Models;
//namespace Programmers.Api
//{
//    public partial class Startup
//    {
//        public void ConfigureAuth(IAppBuilder app)
//        {
//            OAuthAuthorizationServerOptions authServerOptions = new OAuthAuthorizationServerOptions()
//            {
//                //Em produção se atentar que devemos usar HTTPS
//                AllowInsecureHttp = true,
//                TokenEndpointPath = new PathString("/oauth2/token"),
//                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
//                Provider = new CustomOAuthProvider(),
//                AccessTokenFormat = new CustomJwtFormat("http://localhost")
//            };
//            app.UseOAuthAuthorizationServer(authServerOptions);
//        }
//    }
//}