﻿using AutoMapper;
using Programmers.Api;
using Programmers.Api.Model.Request;
using Programmers.Api.Model.Response;
using Programmers.Aplication.Interfaces;
using Programmers.Domain.Entities;
using Programmers.Domain.Validations;
using Programmers.Infra.Context.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Programmers.Api.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly IUserAppService _appService;
        private readonly IUnitOfWork _uow;

        public UserController(IUserAppService appService, IUnitOfWork uow)
        {
            _appService = appService;
            _uow = uow;
        }

        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult Get()
        {
            IEnumerable<UserModelResponse> users;

            try
            {
                users = Mapper.Map<IEnumerable<User>, IEnumerable<UserModelResponse>>(_appService.GetAll());

                if (!users.Any())
                    return NotFound();
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Aconteceu algum erro, tente novamente"));
            }

            return Ok(users);
        }

        [HttpGet]
        [Route("GetById/{id:int}")]
        public IHttpActionResult Get(int id)
        {
            UserModelResponse user;

            try
            {
                user = Mapper.Map<User, UserModelResponse>(_appService.GetById<User>(id));

                if (user == null)
                    return Ok(new ValidationResult().Add("User not found"));
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong, try again later.");
            }

            return Ok(user);
        }

        [HttpPost]
        [Route("Save")]
        public IHttpActionResult Post([FromBody]UserModelRequest userModelRequest)
        {
            ValidationResult validationResult;

            try
            {
                User user = Mapper.Map<UserModelRequest, User>(userModelRequest);

                if (!ModelState.IsValid)
                {
                    return Ok(ResponseErrors.ReturnErrors(user, ModelState));
                }

                validationResult = _appService.Add(user);
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong, try again later.");
            }

            return Ok(validationResult);
        }

        [HttpPut]
        [Route("Update")]
        public IHttpActionResult Put([FromBody]UserModelRequest userModelRequest)
        {
            ValidationResult validationResult;

            try
            {
                User user = Mapper.Map<UserModelRequest, User>(userModelRequest);

                if (!ModelState.IsValid)
                {
                    return Ok(ResponseErrors.ReturnErrors(user, ModelState));
                }

                validationResult = _appService.Update(user);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong, try again later.");
            }

            return Ok(validationResult);
        }

        [HttpDelete]
        [Route("Delete/{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            ValidationResult validationResult;

            try
            {
                User user = _appService.GetById<User>(id);

                if (user == null)
                    return Ok(new ValidationResult().Add("User not found"));

                validationResult = _appService.Delete(user);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong, try again later.");
            }

            return Ok(validationResult);
        }
    }
}
