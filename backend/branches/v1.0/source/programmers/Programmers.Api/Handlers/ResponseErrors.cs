﻿using Programmers.Domain.Validations;

namespace Programmers.Api
{
    public static class ResponseErrors
    {
        public static ValidationResult ReturnErrors(object obj, System.Web.Http.ModelBinding.ModelStateDictionary modelState)
        {
            ValidationResult validationResult = new ValidationResult();

            foreach (var value in modelState.Values)
            {
                foreach (var error in value.Errors)
                {
                    validationResult.Add(error.ErrorMessage);
                }
            }

            return validationResult;
        }
    }
}